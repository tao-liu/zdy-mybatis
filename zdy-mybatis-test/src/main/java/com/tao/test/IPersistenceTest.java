package com.tao.test;

import com.tao.dao.IUserDao;
import com.tao.io.Resources;
import com.tao.pojo.User;
import com.tao.sqlsession.SqlSession;
import com.tao.sqlsession.SqlSessionFactory;
import com.tao.sqlsession.SqlSessionFactoryBuilder;
import org.dom4j.DocumentException;
import org.junit.Before;
import org.junit.Test;

import javax.swing.*;
import java.beans.PropertyVetoException;
import java.io.InputStream;
import java.util.List;


public class IPersistenceTest {

    private SqlSession sqlSession;
    private IUserDao userDao;

    @Before
    public void brFore() throws PropertyVetoException, DocumentException {
        InputStream resourceAsStream = Resources.getResourceAsStream("sqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsStream);
        sqlSession = sqlSessionFactory.openSession();
        userDao = sqlSession.getMapper(IUserDao.class);
    }

    @Test
    public void test() throws Exception {

        //调用
        User user = new User();
        user.setId(1);
        user.setUsername("呵呵");
        /*User user2 = sqlSession.selectOne("user.selectOne", user);
        System.out.println(user2);*/

        List<User> all = userDao.findAll();
        for (User user1 : all) {
            System.out.println(user1);
        }
    }

    @Test
    public void testInsert(){
        User user = new User();
        user.setId(6);
        user.setUsername("啊矮");
        userDao.insertUser(user);

    }

    @Test
    public void testUpdate(){
        User user = new User();
        user.setId(6);
        user.setUsername("大大");
        userDao.updateUser(user);

    }

    @Test
    public void testDelete(){
        User user = new User();
        user.setId(6);
        user.setUsername("大大");
        userDao.deleteUser(user);

    }

}
