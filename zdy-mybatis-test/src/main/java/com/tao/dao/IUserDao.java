package com.tao.dao;

import com.tao.pojo.User;

import java.util.List;

public interface IUserDao {

    //查询所有用户
    public List<User> findAll() throws Exception;

    //根据条件进行查询
    public User findByCondition(User user) throws Exception;

    //增加用户
    public void insertUser(User user);

    //修改用户
    public void updateUser(User user);

    //删除用户
    public void deleteUser(User user);
}
