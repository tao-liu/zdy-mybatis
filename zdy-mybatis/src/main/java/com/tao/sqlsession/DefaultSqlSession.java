package com.tao.sqlsession;

import com.tao.pojo.Configuration;
import com.tao.pojo.MappedStatement;

import java.lang.reflect.*;
import java.util.List;

public class DefaultSqlSession implements SqlSession {

    private Configuration configuration;

    public DefaultSqlSession(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public <E> List<E> selectList(String statementId, Object... params) throws Exception {

        //将要完成对SimpleExecutor里的query方法的调用
        SimpleExecutor simpleExecutor = new SimpleExecutor();
        MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementId);
        List<Object> list = simpleExecutor.query(configuration, mappedStatement, params);
        return (List<E>) list;
    }

    @Override
    public <T> T selectOne(String statementId, Object... params) throws Exception {

        List<Object> objects = selectList(statementId, params);
        if (objects.size() == 1){
            return (T) objects.get(0);
        }else {
            throw new RuntimeException("查询结果为空或返回结果过多");
        }
    }

    @Override
    public void updateTable(String statementId, Object... params) throws Exception {
        SimpleExecutor simpleExecutor = new SimpleExecutor();
        MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementId);
        simpleExecutor.update(configuration, mappedStatement, params);
    }

    @Override
    public <T> T getMapper(Class<?> mapperClass) {
        //使用JDK动态代理来为Dao接口生成代理对象并返回
        Object proxyInstance = Proxy.newProxyInstance(DefaultSqlSession.class.getClassLoader(), new Class[]{mapperClass}, new InvocationHandler() {
            @Override
            public Object invoke(Object o, Method method, Object[] args) throws Throwable {
                //底层都还是去执行JDBC代码   //根据不同情况，来调用selectList或者selectOne
                //准备参数1：statementId：sql语句的唯一标识：namespace.id = 接口全限定名.方法名
                //方法名:findAll
                String methodName = method.getName();
                //接口全限定名
                String className = method.getDeclaringClass().getName();

                String statementId = className + "." + methodName;

                //准备参数2：params : args
                //获取被调用方法的返回值类型
                Type genericReturnType = method.getGenericReturnType();
                //这里为以实现功能为目的，查询方法写死。
                if (methodName.equals("findAll") || methodName.equals("findByCondition")){
                    //判断是否进行了泛型类型参数化
                    if (genericReturnType instanceof ParameterizedType){
                        List<Object> objects = selectList(statementId, args);
                        return objects;
                    }
                    return selectOne(statementId,args);
                }

                updateTable(statementId,args);

                return null;

            }
        });
        return (T) proxyInstance;
    }
}
