package com.tao.sqlsession;

import com.tao.pojo.Configuration;
import com.tao.pojo.MappedStatement;

import java.sql.SQLException;
import java.util.List;

public interface Executor {

    public <E> List<E> query(Configuration configuration, MappedStatement mappedStatement,Object... params) throws SQLException, Exception;

    public void update(Configuration configuration, MappedStatement mappedStatement,Object... params) throws SQLException, Exception;

}
