package com.tao.sqlsession;

public interface SqlSessionFactory {

    public SqlSession openSession();
}
